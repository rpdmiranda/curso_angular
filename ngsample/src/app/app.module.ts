import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListaColaboradoresComponent } from './lista-colaboradores/lista-colaboradores.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaColaboradoresComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
