import { Component } from '@angular/core';
import { IColaborador } from './data/icolaborador';
import { Servidor } from './data/servidor';
import { Terceirizado } from './data/terceirizado';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private colaboradores: IColaborador[] = [
    new Servidor('Jackobson Romualdo Herculano', 123987),
    new Terceirizado('José da Silva Couves',  'Limpeza'),
  ];
}
