import { Component, OnInit, Input } from '@angular/core';
import { IColaborador } from '../data/icolaborador';

@Component({
  selector: 'app-lista-colaboradores',
  templateUrl: './lista-colaboradores.component.html',
  styleUrls: ['./lista-colaboradores.component.css']
})
export class ListaColaboradoresComponent implements OnInit {

  constructor() { }

  @Input()
  private lista: IColaborador[] = [];

  private remover(indice: number): void {
    this.lista.splice(indice, 1);
  }

  ngOnInit() {
  }

}
