import { IColaborador } from "./icolaborador";

export class Terceirizado implements IColaborador {
    constructor(private _nome: string, private _empresa: string) {
    }

    get nomeExibicao(): string {
        return `${this._nome} (${this._empresa})`;
    }

    get tipo(): string {
        return "Terceirizado";
    }
}
