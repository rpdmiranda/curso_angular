import * as $ from 'jquery';

export function jqueryTest(): void {

  new Promise((resolve, reject) => {
    setTimeout(() => resolve(10), 5000);
  }).then((val: number) => {
    console.log(val);
    return (val * 2);
  }).then((val: number) => {
    console.log(val);
    return new Promise((resolve) => {
      setTimeout(() => resolve(val + 1), 1000);
    });
  }).then((val: number) => console.log(val));

  $.ajax('https://jsonplaceholder.typicode.com/posts/1')
    .then(d => console.log(d));
}

