import {Component} from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <div class="container">
      <nav class="navbar navbar-expand-lg bg-dark">
        <span class="navbar-brand">
          {{title}}
        </span>
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" routerLink="/dashboard">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="/heroes">Heroes</a>
          </li>
        </ul>
      </nav>
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
