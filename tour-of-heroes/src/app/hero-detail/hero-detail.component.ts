import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {Location} from '@angular/common';
import {HeroService} from '../hero.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css'],
})
export class HeroDetailComponent implements OnInit {

  private form: FormGroup;

  constructor(private route: ActivatedRoute,
              private location: Location,
              private heroService: HeroService) {

    this.form = new FormGroup({
        'id': new FormControl(),
        'name': new FormControl(),
        'firstSeenDate': new FormControl()
      }
    );
  }

  private heroName: string;

  ngOnInit() {  // Unário "+" para converter para number
    this.route
      .paramMap
      .switchMap((params: ParamMap) => this.heroService.getHero(+params.get('id')))
      .subscribe(hero => {
        this.heroName = hero.name;
        this.form.patchValue(hero);
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    let data = this.form.value;
    this.heroService.updateHero(+data.id, data)
      .then(val => this.location.back());

  }
}
