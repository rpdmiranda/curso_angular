import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeroesComponent} from './my-heroes/my-heroes.component';
import {HeroDetailComponent} from './hero-detail/hero-detail.component';
import {InputDateComponent} from './input-date/input-date.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app/app.component';
import {RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeroService} from './hero.service';

@NgModule({
  declarations: [
    HeroesComponent,
    HeroDetailComponent,
    InputDateComponent,
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'heroes',
        component: HeroesComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'detail/:id',
        component: HeroDetailComponent
      },
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      }
    ])
  ],
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
