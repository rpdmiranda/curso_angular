import {Hero} from './hero';

export const HEROES: Hero[] = [
  new Hero(11,  'Mr. Nice', new Date('2010-01-05')),
  new Hero(12,  'Narco', new Date('2000-01-05')),
  new Hero(13,  'Bombasto', new Date('1990-02-05')),
  new Hero(14,  'Celeritas'),
  new Hero(15,  'Magneta'),
  new Hero(16, 'RubberMan'),
  new Hero(17,  'Dynama'),
  new Hero(18,  'Dr IQ'),
  new Hero(19,  'Magma'),
  new Hero(20,  'Tornado')
];
