import {Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

export const INPUT_DATE_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputDateComponent),
  multi: true
};

@Component({
  selector: 'input-date',
  template: `
    <div class="input-group">
      <input ngbDatepicker class="form-control" [(ngModel)]="model" placeholder="aaaa-mm-dd" #dn="ngbDatepicker"
             (blur)="onBlur()">
      <div class="input-group-addon" (click)="dn.toggle()">
        <i class="fa fa-calendar" aria-hidden="true"></i>
      </div>
    </div>
  `,
  providers: [INPUT_DATE_CONTROL_VALUE_ACCESSOR],
})
export class InputDateComponent implements ControlValueAccessor {

  private _model: NgbDateStruct;
  private onTouchedCallback: () => void;
  private onChangeCallback: (_: any) => void;

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  writeValue(obj: any): void {
    if (obj != null) {
      if (typeof obj === 'string')
        obj = new Date(obj);
      else if (typeof obj === 'number')
        obj = new Date(obj);

      if (obj instanceof Date) {
        this._model = {year: obj.getFullYear(), month: obj.getMonth() + 1, day: obj.getDate()};
      } else {
        this._model = null;
      }
    } else {
      this._model = null;
    }
  }

  onBlur(): void {
    if (this.onTouchedCallback != null)
      this.onTouchedCallback();
  }

  get model(): NgbDateStruct {
    return this._model;
  }

  set model(obj: NgbDateStruct) {
    let newValue: Date;
    if (obj != null && typeof obj === "object" && obj.year != null && obj.month != null && obj.day != null) {
      newValue = new Date(obj.year, obj.month - 1, obj.day);
    } else {
      newValue = null;
    }

    if (this.onChangeCallback != null) {
      this.onChangeCallback(newValue);
    }
  }
}
