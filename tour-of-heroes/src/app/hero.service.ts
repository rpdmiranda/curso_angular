import {Injectable} from '@angular/core';
import {Hero} from './hero';
import {HEROES} from './mock-heroes';

@Injectable()
export class HeroService {

  constructor() {
  }

  private id: number = 100;

  public getHeroes(): Promise<Hero[]> {
    return new Promise(resolve => setTimeout(() => resolve(HEROES), 500));
  }

  public getHero(id: number): Promise<Hero> {
    return this.getHeroes()
      .then(heroes => heroes.find(hero => hero.id === id));
  }

  updateHero(id: number, heroData: any): Promise<any> {
    if (id == null) {
      return this.getHeroes().then(heroes => heroes.push(new Hero(++this.id, heroData.name, heroData.firstSeenDate)));
    } else {
      return this.getHeroes().then(heroes => {
        let hero = heroes.find(hero => hero.id === id);
        if (hero == null)
          return;
        hero.name = heroData.name;
        hero.firstSeenDate = heroData.firstSeenDate;
      });
    }

  }
}
