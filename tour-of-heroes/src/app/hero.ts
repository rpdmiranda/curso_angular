export class Hero {
  constructor(public id: number, public name: string, public firstSeenDate?: Date) {
  }
}
