import {Component, OnInit} from '@angular/core';
import {HeroService} from '../hero.service';
import {Hero} from '../hero';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [HeroService]
})
export class DashboardComponent implements OnInit {

  constructor(private heroService: HeroService) {
  }

  heroes: Hero[];

  ngOnInit() {
    this.heroService.getHeroes().then(h => this.heroes = h.slice(1, 5));
  }

}
