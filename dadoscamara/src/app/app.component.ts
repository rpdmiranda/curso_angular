import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/reduce';
import {Observable} from 'rxjs/Observable';

interface ResumoPartido {
  id: number;
  sigla: string;
  nome: string;
  uri: string;
}

interface Parlamentar {
  uri: string,
  nome: string,
  siglaPartido: string,
  uriPartido: string,
  uf: string,
  idLegislatura: number,
  urlFoto: string
}

interface PartidoStatus {
  data: string,
  idLegislatura: string,
  situacao: string,
  totalPosse: string,
  totalMembros: string,
  uriMembros: string,
  lider: Parlamentar
}

interface Partido {
  id: number,
  sigla: string,
  nome: string,
  uri: string,
  status: PartidoStatus,
  numeroEleitoral: string,
  urlLogo: string,
  urlWebSite: string,
  urlFacebook: string
}

interface Link {
  rel: string,
  href: string
}

interface Result<T> {
  dados: T;
  links: Link[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private http: HttpClient) {
  }

  private partidos: Partido[];

  ngOnInit(): void {
    this.http.get('https://dadosabertos.camara.leg.br/api/v2/partidos?ordenarPor=sigla')
      .map((result: Result<ResumoPartido[]>) => result.dados)
      .concatMap(lista => lista)                                                                // Um observable para cada item do array
      .concatMap((partido: ResumoPartido) => this.http.get(partido.uri) as Observable<Result<Partido>>) // Recupera o partido de cada item da lista
      .map(result => result.dados)
      .reduce((acc, value, index) => acc.concat(value), [] as Partido[])                        // Converte de novo para array
      .do(v => console.log(v))
      .subscribe(partidos => this.partidos = partidos);
  }
}
