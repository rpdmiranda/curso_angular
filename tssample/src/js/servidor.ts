import { IColaborador } from "./icolaborador";

export class Servidor implements IColaborador {
    constructor(private _nome: string, private _matricula: number) {
    }

    get nomeExibicao(): string {
        return `${this._nome} (${this._matricula})`;
    }

    get tipo(): string {
        return "Servidor";
    }
}