import "../css/main.css";

import * as $ from "jquery";

import {IColaborador} from "./icolaborador";
import {Servidor} from "./servidor";
import {Terceirizado} from "./terceirizado";

const colaboradores: IColaborador[] = [
    new Servidor('Jackobson Romualdo Herculano', 123987),
    new Terceirizado("José da Silva Couves",  "Limpeza"),
];

const tabela: JQuery<HTMLElement> = $("#tabela");
const itens: JQuery<HTMLElement> = tabela.find("tbody");

function redesenharTabela(): void {
    itens.find("tr").remove();

    let i: number = 0;
    for (const colaborador of colaboradores) {
        let linha: string = `<tr>`;
        linha = linha + `<td>${colaborador.nomeExibicao}</td>`;
        linha = linha + `<td>${colaborador.tipo}</td>`;
        linha = linha + `<td><button data-ordem='${i++}'>-</button></td>`;
        linha = linha + '</tr>';
        itens.append(linha);
    }
}

redesenharTabela();

$(itens).on('click', 'button', (event: JQuery.Event) => {
    const indice: number = Number($(event.target).data("ordem"));
    colaboradores.splice(indice, 1);
    redesenharTabela();
});
